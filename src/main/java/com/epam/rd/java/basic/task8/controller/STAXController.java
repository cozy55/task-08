package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private List<Flower> flowerList;
	private Flower flower;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		this.flowerList = new ArrayList<>();
	}

	public void parseXML(){
		XMLInputFactory factory = XMLInputFactory.newInstance();
		try {
			XMLEventReader xmlEventReader = factory.createXMLEventReader(new FileInputStream(xmlFileName));

			while (xmlEventReader.hasNext()){
				XMLEvent xmlEvent = xmlEventReader.nextEvent();
				if(xmlEvent.isStartElement()){
					StartElement startElement = xmlEvent.asStartElement();
					switch (startElement.getName().getLocalPart()){
						case "flower":
							flower = new Flower();
							break;
						case "name":
							xmlEvent = xmlEventReader.nextEvent();
							flower.setName(xmlEvent.asCharacters().getData());
							break;
						case "soil":
							xmlEvent = xmlEventReader.nextEvent();
							flower.setSoil(xmlEvent.asCharacters().getData());
							break;
						case "origin":
							xmlEvent = xmlEventReader.nextEvent();
							flower.setOrigin(xmlEvent.asCharacters().getData());
							break;
						case "stemColour":
							xmlEvent = xmlEventReader.nextEvent();
							flower.setStemColour(xmlEvent.asCharacters().getData());
							break;
						case "leafColour":
							xmlEvent = xmlEventReader.nextEvent();
							flower.setLeafColour(xmlEvent.asCharacters().getData());
							break;
						case "aveLenFlower":
							xmlEvent = xmlEventReader.nextEvent();
							flower.setAveLengthFlower(Integer.parseInt(xmlEvent.asCharacters().getData()));
							break;
						case "tempreture":
							xmlEvent = xmlEventReader.nextEvent();
							flower.setTempreture(Integer.parseInt(xmlEvent.asCharacters().getData()));
							break;
						case "lighting":
							Attribute lightRequiringAttr = startElement.getAttributeByName(new QName("lightRequiring"));
							if(lightRequiringAttr != null) {
								flower.setLighting(lightRequiringAttr.getValue());
							}
							break;
						case "watering":
							xmlEvent = xmlEventReader.nextEvent();
							flower.setWatering(Integer.parseInt(xmlEvent.asCharacters().getData()));
							break;
						case "multiplying":
							xmlEvent = xmlEventReader.nextEvent();
							flower.setMultiplying(xmlEvent.asCharacters().getData());
							break;
					}
				}
				if(xmlEvent.isEndElement()){
					EndElement endElement = xmlEvent.asEndElement();
					if(endElement.getName().getLocalPart().equals("flower")){
						flowerList.add(flower);
					}
				}

			}
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void sortElements(){
		flowerList.sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				return o1.getWatering() - o2.getWatering();
			}
		});
	}

	public void save(String outputXmlFile){
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLEventFactory eventFactory = XMLEventFactory.newInstance();
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			XMLEventWriter eventWriter = outputFactory.createXMLEventWriter(output);

			eventWriter.add(eventFactory.createStartDocument());
			eventWriter.add(eventFactory.createStartElement("", "", "flowers"));
			eventWriter.add(eventFactory.createAttribute("xmlns", "http://www.nure.ua"));
			//eventWriter.add(eventFactory.createNamespace("xmlns", "http://www.nure.ua"));
			eventWriter.add(eventFactory.createAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance"));
			eventWriter.add(eventFactory.createAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd "));

			for(Flower f : flowerList){
				addFlowerElement(f, eventWriter);
			}

			eventWriter.add(eventFactory.createEndElement("", "", "flowers"));
			eventWriter.add(eventFactory.createEndDocument());

			eventWriter.flush();
			eventWriter.close();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}

		try {
			String xml = formatXML(output.toString());

			Files.writeString(Paths.get(outputXmlFile),
					xml);

		} catch (TransformerException | IOException e) {
			e.printStackTrace();
		}

	}

	private static String formatXML(String xml) throws TransformerException {

		// write data to xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		// pretty print by indention
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		// add standalone="yes", add line break before the root element
		transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

		StreamSource source = new StreamSource(new StringReader(xml));
		StringWriter output = new StringWriter();
		transformer.transform(source, new StreamResult(output));

		return output.toString();

	}

	private void addFlowerElement(Flower f, XMLEventWriter eventWriter) throws XMLStreamException {
		XMLEventFactory eventFactory = XMLEventFactory.newInstance();

		eventWriter.add(eventFactory.createStartElement("", "", "flower"));

		eventWriter.add(eventFactory.createStartElement("", "", "name"));
		eventWriter.add(eventFactory.createCharacters(f.getName()));
		eventWriter.add(eventFactory.createEndElement("", "", "name"));

		eventWriter.add(eventFactory.createStartElement("", "", "soil"));
		eventWriter.add(eventFactory.createCharacters(f.getSoil()));
		eventWriter.add(eventFactory.createEndElement("", "", "soil"));

		eventWriter.add(eventFactory.createStartElement("", "", "origin"));
		eventWriter.add(eventFactory.createCharacters(f.getOrigin()));
		eventWriter.add(eventFactory.createEndElement("", "", "origin"));

		eventWriter.add(eventFactory.createStartElement("", "", "visualParameters"));

		eventWriter.add(eventFactory.createStartElement("", "", "stemColour"));
		eventWriter.add(eventFactory.createCharacters(f.getStemColour()));
		eventWriter.add(eventFactory.createEndElement("", "", "stemColour"));

		eventWriter.add(eventFactory.createStartElement("", "", "leafColour"));
		eventWriter.add(eventFactory.createCharacters(f.getLeafColour()));
		eventWriter.add(eventFactory.createEndElement("", "", "leafColour"));

		eventWriter.add(eventFactory.createStartElement("", "", "aveLenFlower"));
		eventWriter.add(eventFactory.createAttribute("measure", "cm"));
		eventWriter.add(eventFactory.createCharacters(String.valueOf(f.getAveLengthFlower())));
		eventWriter.add(eventFactory.createEndElement("", "", "aveLenFlower"));

		eventWriter.add(eventFactory.createEndElement("", "", "visualParameters"));

		eventWriter.add(eventFactory.createStartElement("", "", "growingTips"));

		eventWriter.add(eventFactory.createStartElement("", "", "tempreture"));
		eventWriter.add(eventFactory.createAttribute("measure", "celcius"));
		eventWriter.add(eventFactory.createCharacters(String.valueOf(f.getTempreture())));
		eventWriter.add(eventFactory.createEndElement("", "", "tempreture"));

		eventWriter.add(eventFactory.createStartElement("", "", "lighting"));
		eventWriter.add(eventFactory.createAttribute("lightRequiring", f.isLighting()));
		eventWriter.add(eventFactory.createEndElement("", "", "lighting"));

		eventWriter.add(eventFactory.createStartElement("", "", "watering"));
		eventWriter.add(eventFactory.createAttribute("measure", "mlPerWeek"));
		eventWriter.add(eventFactory.createCharacters(String.valueOf(f.getWatering())));
		eventWriter.add(eventFactory.createEndElement("", "", "watering"));

		eventWriter.add(eventFactory.createEndElement("", "", "growingTips"));

		eventWriter.add(eventFactory.createStartElement("", "", "multiplying"));
		eventWriter.add(eventFactory.createCharacters(f.getMultiplying()));
		eventWriter.add(eventFactory.createEndElement("", "", "multiplying"));

		eventWriter.add(eventFactory.createEndElement("", "", "flower"));
	}

}