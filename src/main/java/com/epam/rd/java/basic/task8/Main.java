package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		//String outputXmlFile;
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.parseXML();

		// sort (case 1)
		domController.sortElements();

		// save
		String outputXmlFile = "output.dom.xml";
		domController.save(outputXmlFile);
		// PLACE YOUR CODE HERE

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parseXML();
		
		// sort  (case 2)
		saxController.sortElements();

		// save
		outputXmlFile = "output.sax.xml";
		saxController.save(outputXmlFile);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parseXML();
		
		// sort  (case 3)
        staxController.sortElements();

		// save
		outputXmlFile = "output.stax.xml";
		staxController.save(outputXmlFile);

	}

}
