package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private List<Flower> flowerList;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		this.flowerList = new ArrayList<>();
	}

	public void parseXML() {
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			parser.parse(new File(xmlFileName), new XMLHandler());
			flowerList.forEach(System.out::println);
		}catch (ParserConfigurationException | SAXException | IOException e){
			e.printStackTrace();
		}
	}

	public void sortElements(){
		flowerList.sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				return o1.getTempreture() - o2.getTempreture();
			}
		});
	}

	public void save(String outputXmlFile){
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLEventFactory eventFactory = XMLEventFactory.newInstance();
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			XMLEventWriter eventWriter = outputFactory.createXMLEventWriter(output);

			eventWriter.add(eventFactory.createStartDocument());
			eventWriter.add(eventFactory.createStartElement("", "", "flowers"));
			eventWriter.add(eventFactory.createAttribute("xmlns", "http://www.nure.ua"));
			//eventWriter.add(eventFactory.createNamespace("xmlns", "http://www.nure.ua"));
			eventWriter.add(eventFactory.createAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance"));
			eventWriter.add(eventFactory.createAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd "));

			for(Flower f : flowerList){
				addFlowerElement(f, eventWriter);
			}

			eventWriter.add(eventFactory.createEndElement("", "", "flowers"));
			eventWriter.add(eventFactory.createEndDocument());

			eventWriter.flush();
			eventWriter.close();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}

		try {
			String xml = formatXML(output.toString());

			Files.writeString(Paths.get(outputXmlFile),
					xml);

		} catch (TransformerException | IOException e) {
			e.printStackTrace();
		}

	}

	private static String formatXML(String xml) throws TransformerException {

		// write data to xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		// pretty print by indention
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		// add standalone="yes", add line break before the root element
		transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

		StreamSource source = new StreamSource(new StringReader(xml));
		StringWriter output = new StringWriter();
		transformer.transform(source, new StreamResult(output));

		return output.toString();

	}

	private void addFlowerElement(Flower f, XMLEventWriter eventWriter) throws XMLStreamException {
		XMLEventFactory eventFactory = XMLEventFactory.newInstance();

		eventWriter.add(eventFactory.createStartElement("", "", "flower"));

		eventWriter.add(eventFactory.createStartElement("", "", "name"));
		eventWriter.add(eventFactory.createCharacters(f.getName()));
		eventWriter.add(eventFactory.createEndElement("", "", "name"));

		eventWriter.add(eventFactory.createStartElement("", "", "soil"));
		eventWriter.add(eventFactory.createCharacters(f.getSoil()));
		eventWriter.add(eventFactory.createEndElement("", "", "soil"));

		eventWriter.add(eventFactory.createStartElement("", "", "origin"));
		eventWriter.add(eventFactory.createCharacters(f.getOrigin()));
		eventWriter.add(eventFactory.createEndElement("", "", "origin"));

		eventWriter.add(eventFactory.createStartElement("", "", "visualParameters"));

		eventWriter.add(eventFactory.createStartElement("", "", "stemColour"));
		eventWriter.add(eventFactory.createCharacters(f.getStemColour()));
		eventWriter.add(eventFactory.createEndElement("", "", "stemColour"));

		eventWriter.add(eventFactory.createStartElement("", "", "leafColour"));
		eventWriter.add(eventFactory.createCharacters(f.getLeafColour()));
		eventWriter.add(eventFactory.createEndElement("", "", "leafColour"));

		eventWriter.add(eventFactory.createStartElement("", "", "aveLenFlower"));
		eventWriter.add(eventFactory.createAttribute("measure", "cm"));
		eventWriter.add(eventFactory.createCharacters(String.valueOf(f.getAveLengthFlower())));
		eventWriter.add(eventFactory.createEndElement("", "", "aveLenFlower"));

		eventWriter.add(eventFactory.createEndElement("", "", "visualParameters"));

		eventWriter.add(eventFactory.createStartElement("", "", "growingTips"));

		eventWriter.add(eventFactory.createStartElement("", "", "tempreture"));
		eventWriter.add(eventFactory.createAttribute("measure", "celcius"));
		eventWriter.add(eventFactory.createCharacters(String.valueOf(f.getTempreture())));
		eventWriter.add(eventFactory.createEndElement("", "", "tempreture"));

		eventWriter.add(eventFactory.createStartElement("", "", "lighting"));
		eventWriter.add(eventFactory.createAttribute("lightRequiring", f.isLighting()));
		eventWriter.add(eventFactory.createEndElement("", "", "lighting"));

		eventWriter.add(eventFactory.createStartElement("", "", "watering"));
		eventWriter.add(eventFactory.createAttribute("measure", "mlPerWeek"));
		eventWriter.add(eventFactory.createCharacters(String.valueOf(f.getWatering())));
		eventWriter.add(eventFactory.createEndElement("", "", "watering"));

		eventWriter.add(eventFactory.createEndElement("", "", "growingTips"));

		eventWriter.add(eventFactory.createStartElement("", "", "multiplying"));
		eventWriter.add(eventFactory.createCharacters(f.getMultiplying()));
		eventWriter.add(eventFactory.createEndElement("", "", "multiplying"));

		eventWriter.add(eventFactory.createEndElement("", "", "flower"));
	}

	private class XMLHandler extends DefaultHandler{
		private String currentElement;
		private Flower currentFlower;

		@Override
		public void startDocument() throws SAXException {
			currentFlower = new Flower();
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			currentElement = qName;
			if(qName.equals("lighting")){
				currentFlower.setLighting(attributes.getValue("lightRequiring"));
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if(currentFlower != null && !currentFlower.toString().contains("'null'")) {
				flowerList.add(currentFlower);
				currentFlower = new Flower();
			}
			currentElement = "";
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			String value = new String(ch, start, length).replace("\n", "").trim();;

			if(currentElement.equals("name")){
				currentFlower.setName(value);
			}
			if(currentElement.equals("soil")){
				currentFlower.setSoil(value);
			}
			if(currentElement.equals("origin")){
				currentFlower.setOrigin(value);
			}
			if(currentElement.equals("stemColour")){
				currentFlower.setStemColour(value);
			}
			if(currentElement.equals("leafColour")){
				currentFlower.setLeafColour(value);
			}
			if(currentElement.equals("aveLenFlower")){
				currentFlower.setAveLengthFlower(Integer.parseInt(value));
			}
			if(currentElement.equals("tempreture")){
				currentFlower.setTempreture(Integer.parseInt(value));
			}
			if(currentElement.equals("watering")){
				currentFlower.setWatering(Integer.parseInt(value));
			}
			if(currentElement.equals("multiplying")){
				currentFlower.setMultiplying(value);
			}
		}
	}
	// PLACE YOUR CODE HERE

}