package com.epam.rd.java.basic.task8;

public class Flower {
    private String name;
    private String origin;
    private String soil;

    private String stemColour;
    private String leafColour;
    private int aveLengthFlower;

    private int tempreture;
    private String lighting;
    private int watering;

    private String multiplying;

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", origin='" + origin + '\'' +
                ", solid='" + soil + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLengthFlower=" + aveLengthFlower +
                ", tempreture=" + tempreture +
                ", lighting=" + lighting +
                ", watering=" + watering +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String solid) {
        this.soil = solid;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public int getAveLengthFlower() {
        return aveLengthFlower;
    }

    public void setAveLengthFlower(int aveLengthFlower) {
        this.aveLengthFlower = aveLengthFlower;
    }

    public int getTempreture() {
        return tempreture;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public String isLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }
}