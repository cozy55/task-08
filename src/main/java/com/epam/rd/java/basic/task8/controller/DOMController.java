package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private List<Flower> flowerList;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		flowerList = new ArrayList<>();
	}

	public void parseXML() throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(xmlFileName);
		NodeList flowerElements = document.getDocumentElement().getElementsByTagName("flower");

		System.out.println(flowerElements.getLength());

		for(int i = 0; i < flowerElements.getLength(); i++){
			flowerList.add(getFlower(flowerElements.item(i)));
		}
	}

	public void save(String outputXmlFile) throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.newDocument();

		Element flowers = document.createElement("flowers");
		flowers.setAttribute("xmlns", "http://www.nure.ua");
		flowers.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		flowers.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
		document.appendChild(flowers);

		for(Flower f : flowerList){
			flowers.appendChild(parseFlowerToElement(f, document));
		}


		try (FileOutputStream output =
					 new FileOutputStream(outputXmlFile)) {
			writeXml(document, output);
		} catch (IOException | TransformerException e) {
			e.printStackTrace();
		}

	}

	private Element parseFlowerToElement(Flower flower, Document document){
		Element flowerElement = document.createElement("flower");

		Element name = document.createElement("name");
		name.setTextContent(flower.getName());
		flowerElement.appendChild(name);

		Element soil = document.createElement("soil");
		soil.setTextContent(flower.getSoil());
		flowerElement.appendChild(soil);

		Element origin = document.createElement("origin");
		origin.setTextContent(flower.getOrigin());
		flowerElement.appendChild(origin);

		Element visualParameters = document.createElement("visualParameters");

		Element stemColour = document.createElement("stemColour");
		stemColour.setTextContent(flower.getStemColour());

		Element leafColour = document.createElement("leafColour");
		leafColour.setTextContent(flower.getLeafColour());

		Element aveLenFlower = document.createElement("aveLenFlower");
		aveLenFlower.setAttribute("measure", "cm");
		aveLenFlower.setTextContent(String.valueOf(flower.getAveLengthFlower()));

		visualParameters.appendChild(stemColour);
		visualParameters.appendChild(leafColour);
		visualParameters.appendChild(aveLenFlower);
		flowerElement.appendChild(visualParameters);

		Element growingTips = document.createElement("growingTips");

		Element tempreture = document.createElement("tempreture");
		tempreture.setAttribute("measure", "celcius");
		tempreture.setTextContent(String.valueOf(flower.getTempreture()));

		Element lighting = document.createElement("lighting");
		lighting.setAttribute("lightRequiring", flower.isLighting());

		Element watering = document.createElement("watering");
		watering.setAttribute("measure", "mlPerWeek");
		watering.setTextContent(String.valueOf(flower.getWatering()));

		growingTips.appendChild(tempreture);
		growingTips.appendChild(lighting);
		growingTips.appendChild(watering);
		flowerElement.appendChild(growingTips);

		Element multiplying = document.createElement("multiplying");
		multiplying.setTextContent(flower.getMultiplying());
		flowerElement.appendChild(multiplying);

		return flowerElement;
	}

	private static void writeXml(Document doc,
								 OutputStream output)
			throws TransformerException {

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(output);

		transformer.transform(source, result);

	}

	public List<Flower> getFlowerList() {
		return flowerList;
	}

	public void sortElements(){
		flowerList.sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
	}

	private Flower getFlower(Node node){
		Flower flower = new Flower();
		if(node.getNodeType() == Node.ELEMENT_NODE){
			Element element = (Element) node;
			flower.setName(getTagValue("name", element));
			flower.setSoil(getTagValue("soil", element));
			flower.setOrigin(getTagValue("origin", element));
			flower.setStemColour(getTagValue("stemColour", element));
			flower.setLeafColour(getTagValue("leafColour", element));
			flower.setAveLengthFlower(Integer.parseInt(getTagValue("aveLenFlower", element)));
			flower.setTempreture(Integer.parseInt(getTagValue("tempreture", element)));
			flower.setLighting(getTagAttributeValue("lighting", "lightRequiring", element));
			flower.setWatering(Integer.parseInt(getTagValue("watering", element)));
			flower.setMultiplying(getTagValue("multiplying", element));
		}
		return flower;
	}

	private String getTagValue(String name, Element element){
		return element.getElementsByTagName(name).item(0).getTextContent();
	}

	private String getTagAttributeValue(String tag, String attribute, Element element){
		return element.getElementsByTagName(tag).item(0).getAttributes().getNamedItem(attribute).getNodeValue();
	}
}
